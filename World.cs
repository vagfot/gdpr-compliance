﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTaskFramework;
using VRageMath;
using System.Threading;
using System.Diagnostics;
using System.Collections.Concurrent;
using ParallelTasks;

namespace TestingTask
{
    class State
    {
        public TimeSpan deltaTime;
        public volatile int start;
        public volatile int stop;
        public volatile WorldObject[] objectsArray;
        public BoundingBox box;
        public volatile ConcurrentStack<WorldObject> resultStack;
        public volatile ManualResetEvent manualEvent;
    }
    
    // TODO: World is really slow now, optimize it.
    // TODO: Fix excessive allocations during run.
    // TODO: Write body of 'PreciseCollision' method.
    class World : IMyWorld
    {
        static int workItemCount = 0;
        volatile List<WorldObject> m_objects = new List<WorldObject>();
        public volatile WorldObject[] objectsArray;
        public ConcurrentStack<WorldObject> resultStack;       
        private const int partitions = 10;
        ManualResetEvent[] manualEvents = new ManualResetEvent[partitions+1];
        /// <summary>
        /// Time of the world, increased with each update.
        /// </summary>
        public TimeSpan Time { get; private set; }

        public Vector3 ClosestAsteroid(WorldObject ship)
        {
            float minDistance = float.PositiveInfinity;
            Vector3 closestAsteroid = objectsArray[0].Position;

            for (int i = 0; i < objectsArray.Length; i++)
            {
                try
                {
                    if (objectsArray[i] != ship)
                    {
                        float distance = ship.BoundingBox.Distance(objectsArray[i].Position);
                        if (distance < minDistance)
                        {
                            distance = minDistance;
                            closestAsteroid = objectsArray[i].Position;
                        }
                    }
                }
                catch (Exception e)
                {
                    //if (closestAsteroid == null)
                    //    closestAsteroid = Vector3.Forward;
                    Console.WriteLine(e.Message);
                }

            }
            return closestAsteroid;
        }
        /// <summary>
        /// Adds new object into world.
        /// World is responsible for calling OnAdded method on object when object is added.
        /// </summary>
        public void Add(WorldObject obj)
        {
            
                obj.OnAdded(this);
                m_objects.Add(obj);
            
        }

        /// <summary>
        /// Removes object from world.
        /// World is responsible for calling OnRemoved method on object when object is removed.
        /// </summary>
        public void Remove(WorldObject obj)
        {

            try
            {
                m_objects.Remove(obj);
                obj.OnRemoved();
                Console.WriteLine(m_objects.Count);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                GC.Collect();

            }
        }

        /// <summary>
        /// Called when object is moved in the world.
        /// </summary>
        public void OnObjectMoved(WorldObject obj, Vector3 displacement)
        {
        }

        /// <summary>
        /// Clears whole world and resets the time.
        /// </summary>
        public void Clear()
        {
            Time = TimeSpan.Zero;
            m_objects.Clear();            
        }

        /// <summary>
        /// Queries the world for objects in a box. Matching objects are added into result list.
        /// Query should return all overlapping objects.
        /// </summary>
        public void Query(BoundingBox box, List<WorldObject> resultList)
        {
            

                //objectsArray = new ConcurrentStack<WorldObject>(m_objects);

                //resultStack = new ConcurrentStack<WorldObject>(resultList);
 

                objectsArray = m_objects.ToArray<WorldObject>();

                    for (int i = 0; i < objectsArray.Length; i++)
                    {
                        try
                        {
                            if (box.Contains(objectsArray[i].BoundingBox) != ContainmentType.Disjoint)
                                resultList.Add(objectsArray[i]);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        
                    }

                    

                        //var lastState = new State();
                        //lastState.start = partitions * (objectsArray.Count / partitions);
                        //lastState.stop = ((objectsArray.Count % partitions) + partitions * (objectsArray.Count / partitions));
                        //if (lastState.stop != lastState.start)
                        //{
                        //    lastState.objectsArray = objectsArray;
                        //    lastState.box = box;
                        //    lastState.resultStack = resultStack;
                        //    manualEvents[partitions] = new ManualResetEvent(false);
                        //    lastState.manualEvent = manualEvents[partitions];
                        //    lock (objectsArray)
                        //    {
                                
                        //        ThreadPool.QueueUserWorkItem(new WaitCallback(QParallel), lastState);
                        //        manualEvents[partitions].WaitOne(); 
                        //    }
                        //}
                        //for (int i = 0; i < partitions; i++)
                        //{

                        //    var state = new State();
                        //    state.start = i * (objectsArray.Count / partitions);
                        //    state.stop = (i + 1) * (objectsArray.Count / partitions);
                        //    state.objectsArray = objectsArray;
                        //    state.box = box;
                        //    state.resultStack = resultStack;
                        //    manualEvents[i] = new ManualResetEvent(false);
                        //    state.manualEvent = manualEvents[i];
                        //    lock (objectsArray)
                        //    {
                                 
                        //        ThreadPool.QueueUserWorkItem(new WaitCallback(QParallel), state);
                        //        manualEvents[i].WaitOne();
                        //    }

                        //}
                        
                        //resultList = resultStack.ToList<WorldObject>();
        }

        
        private void QParallel(object state)
        {
            int workItemNumber = workItemCount;
            Interlocked.Increment(ref workItemCount);
            for (int k = ((State)state).start; k < ((State)state).stop; k++)
            {
                try
                {
                    if (((State)state).box.Contains(objectsArray.ElementAt(k).BoundingBox) != ContainmentType.Disjoint)
                        resultStack.Push(objectsArray.ElementAt(k));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally { ((State)state).manualEvent.Set(); }
            }
        }
        
        /// <summary>
        /// Updates the world in following order:
        /// 1. Increase time.
        /// 2. Call Update on all objects with NeedsUpdate flag.
        /// 3. Call PostUpdate on all objects with NeedsUpdate flag.
        /// PostUpdate on first object must be called when all other objects are Updated.
        /// </summary>
        public void Update(TimeSpan deltaTime)
        {
            
                try
                {
                    Time += deltaTime;

                        objectsArray = m_objects.ToArray<WorldObject>();
                        var lastState = new State();
                        lastState.deltaTime = deltaTime;
                        lastState.start = partitions * (objectsArray.Length / partitions);
                        lastState.stop = ((objectsArray.Length % partitions) + partitions * (objectsArray.Length / partitions));
                        if (lastState.stop != lastState.start)
                        {
                            manualEvents[partitions] = new ManualResetEvent(false);
                            lastState.manualEvent = manualEvents[partitions];
                            lastState.objectsArray = objectsArray;
                            lock (objectsArray)
                            {                               
                                ThreadPool.QueueUserWorkItem(new WaitCallback(ParallelUpdate), lastState);
                                manualEvents[partitions].WaitOne();
                            }
                        }
                        for (int i = 0; i < partitions; i++)
                        {

                            var state = new State();
                            state.deltaTime = deltaTime;
                            state.start = i * (objectsArray.Length / partitions);
                            state.stop = (i + 1) * (objectsArray.Length / partitions);
                            state.objectsArray = objectsArray;
                            manualEvents[i] = new ManualResetEvent(false);
                            state.manualEvent = manualEvents[i];
                            lock (objectsArray)
                            {                                
                                ThreadPool.QueueUserWorkItem(new WaitCallback(ParallelUpdate), state);
                                manualEvents[i].WaitOne();
                            }
                            
                        }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                //finally { GC.Collect(); }
        }


        
        private void ParallelUpdate(object state)
        {
            State s = (State)state;
            int workItemNumber = workItemCount;
            Interlocked.Increment(ref workItemCount);
            for (int i = s.start; i < s.stop; i++)
            {
                if (s.objectsArray[i] != null) 
                {
                    try
                    {
                        
                            if (s.objectsArray[i].NeedsUpdate)
                            {

                                s.objectsArray[i].Update(s.deltaTime);
                                s.objectsArray[i].PostUpdate();
                            }
                        
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally { ((State)state).manualEvent.Set(); }
                    
                }
            }

        }

        /// <summary>
        /// Calculates precise collision of two moving objects.
        /// Returns exact delta time of touch (e.g. 1 is one second in future from now).
        /// When objects are already touching or overlapping, returns zero. When the objects won't ever touch, returns positive infinity.
        /// </summary>
        public float PreciseCollision(WorldObject a, WorldObject b)
        {
            return float.PositiveInfinity;
        }

        //public Node GetNode(Vector3 pos)
        //{
        //    Vector3 roundedPos = new Vector3(Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y), Mathf.FloorToInt(pos.z));
        //    float minDist = float.PositiveInfinity;
        //    Node closestNode = null;
        //    for (int i = 0; i < gridLength; i++)
        //    {
        //        for (int j = 0; j < gridWidth; j++)
        //        {
        //            float sqrDist = (grid[i, j].pos - roundedPos).sqrMagnitude;
        //            if (sqrDist < minDist)
        //            {
        //                minDist = sqrDist;
        //                closestNode = grid[i, j];
        //            }
        //        }
        //    }
        //    return closestNode;
        //}
    }
}
