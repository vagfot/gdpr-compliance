﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTaskFramework;
using VRageMath;

namespace TestingTask
{
    public interface IMyWorld: IWorld
    {
        Vector3 ClosestAsteroid(WorldObject ship);
    }
}
